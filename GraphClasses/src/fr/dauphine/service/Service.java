package fr.dauphine.service;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import fr.dauphine.service.Constants.EXECUTION_STATE;

/**
* This class represents a Web Service vertex in the graph.
* @author Rafael Angarita
* @version 1.0
*/
public class Service implements Serializable, Comparable<Service> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private List<Data> inputs;
	private List<Data> outputs;
	private double outputDependency = -1.0;
	private URI uri;
	private double estimatedExecutionTime = 0.0;
	private double availability = 1;
	private double price = 0.0;
	private EXECUTION_STATE executionState;
	private TransactionalProperty transactionalProperty  = TransactionalProperty.None;
	
	
	public Service(String name) {
		this.name = name;
		inputs = new ArrayList<Data>();
		outputs = new ArrayList<Data>();
		executionState = EXECUTION_STATE.INITIAL;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getEstimatedExecutionTime() {
		return this.estimatedExecutionTime;
	}
	
	public void setAvailability(double availability) {
		this.availability = availability;
	}
	
	public double getAvailability() {
		return this.availability;
	}
	
	public void setEstimatedExecutionTime(double e) {
		
		this.estimatedExecutionTime = e;
	}
	
	public List<Data> getInputs() {
		return inputs;
	}
	
	public void setInputs(List<Data> inputs) {
		this.inputs = inputs;
	}
	
	public List<Data> getOutputs() {
		return outputs;
	}
	
	public void setOutputs(List<Data> outputs) {
		this.outputs = outputs;
	}
	
	public void addOutput(Data output) {
		if(this.outputs == null) {
			this.outputs = new ArrayList<Data>();
		}
		this.outputs.add(output);
	}
	
	public void addInput(Data input) {
		if(this.inputs == null) {
			this.inputs = new ArrayList<Data>();
		}
		this.inputs.add(input);
	}
	
	public double getOutputDependency() {
		return outputDependency;
	}
	
	public void setOutputDependency(double outputDependency) {
		this.outputDependency = outputDependency;
	}
	
	public boolean isControl() {
		return name.equals(Constants.INITIAL_NODE) || name.equals(Constants.FINAL_NODE);
	}
	
	public boolean isInitial() {
		return name.equals(Constants.INITIAL_NODE);
	}
	
	public boolean isFinal() {
		return name.equals(Constants.FINAL_NODE);
	}
	
	
	@Override
	public String toString() {
		//return "{name: " + name + ", inputs:" + inputs + ", outputs: " + outputs 
		//		+ ", tp:" + transactionalProperty + "}";
		return name;
	}
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Service) {
			Service tmp = (Service) obj;
			return name.equals(tmp.name);
		}
		
		return false;
	}
	
	
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return name.hashCode();
	}
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	@Override
	public int compareTo(Service o) {
		return name.compareTo(o.getName());
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public EXECUTION_STATE getExecutionState() {
		return executionState;
	}
	public void setExecutionState(EXECUTION_STATE executionState) {
		this.executionState = executionState;
	}
	public TransactionalProperty getTransactionalProperty() {
		return transactionalProperty;
	}
	public void setTransactionalProperty(TransactionalProperty transactionalProperty) {
		this.transactionalProperty = transactionalProperty;
	}
	public boolean isRetriable() {
		return transactionalProperty.equals(TransactionalProperty.CompensableRetriable)
				|| transactionalProperty.equals(TransactionalProperty.PivotRetriable);
	}
	
	public boolean isAbandoned() {
		return getExecutionState().equals(EXECUTION_STATE.ABANDONED)?true:false;
	}
	
	public boolean isExecuted() {
		return getExecutionState().equals(EXECUTION_STATE.EXECUTED)?true:false;
	}
	
	public boolean isInitialState() {
		return getExecutionState().equals(EXECUTION_STATE.INITIAL)?true:false;
	}
	
	public void addInputs(List<Data> inputs) {
		this.inputs.addAll(inputs);
	}
	
	public void addOutputs(List<Data> outputs) {
		this.outputs.addAll(outputs);
	}
	
	
}