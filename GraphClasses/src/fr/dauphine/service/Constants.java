package fr.dauphine.service;

public final class Constants {
	public static final String INITIAL_NODE = "wsci";
	public static final String FINAL_NODE = "wscf";
	public enum EXECUTION_STATE {
		RUNNING, EXECUTED, COMPENSATED,
		INITIAL, ABANDONED, RUNNING_COMPENSATION,
		CHECKPOINTED, SKIPPED,
		//non-fault tolerant states
		FAILED
	}
}