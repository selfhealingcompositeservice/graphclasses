package fr.dauphine.service;

public enum TransactionalProperty {
	None,
    Pivot,
    Compensable,
    PivotRetriable,
    CompensableRetriable
}


