package fr.dauphine.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum CompositeTransactionalProperty {
	None,
    Atomic,
    Compensable,
    AtomicRetriable,
    CompensableRetriable;
    
    private static final List<CompositeTransactionalProperty> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    
    private static final Random RANDOM = new Random();

    public static CompositeTransactionalProperty randomCompositeTP()  {
      return VALUES.get(RANDOM.nextInt((VALUES.size()-1- 1) + 1) + 1);
    }
}


