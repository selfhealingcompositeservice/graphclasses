package fr.dauphine.utils;

import java.util.ArrayList;
import java.util.List;

import fr.dauphine.service.Data;

public class Utils {

	public static List<Data> asDataList(List<String> list) {
		List<Data> data = new ArrayList<Data>();
		
		for(String s:list)
			data.add(new Data(s));
		
		return data;
	}
	
	public static List<String> asStringList(List<Data> list) {
		List<String> data = new ArrayList<String>();
		
		for(Data s:list)
			data.add(s.getName());
		
		return data;
	}
}
