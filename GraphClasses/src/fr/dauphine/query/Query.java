package fr.dauphine.query;

import java.util.ArrayList;
import java.util.List;

import fr.dauphine.service.Data;
import fr.dauphine.service.TransactionalProperty;

public class Query {
	
	private List<Data> inputs;
	private List<Data> outputs;
	private TransactionalProperty transactionalProperty;
	
	public List<Data> getInputs() {
		return inputs;
	}
	public void setInputs(List<Data> inputs) {
		this.inputs = inputs;
	}
	public List<Data> getOutputs() {
		return outputs;
	}
	public void setOutputs(List<Data> outputs) {
		this.outputs = outputs;
	}
	public TransactionalProperty getTransactionalProperty() {
		return transactionalProperty;
	}
	public void setTransactionalProperty(TransactionalProperty transactionalProperty) {
		this.transactionalProperty = transactionalProperty;
	}
	public void addInput(Data input) {
		if(inputs == null) {
			inputs = new ArrayList<Data>();
		}
		
		inputs.add(input);
		
	}
	
	public void addOutput(Data output) {
		if(outputs == null) {
			outputs = new ArrayList<Data>();
		}
		
		outputs.add(output);
		
	}

}
